<?php

namespace App\DataFixtures;

use App\Entity\Catalog;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    const USER_ADMIN = 'deiberal@gmail.com';
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->encoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $this->createUsers($manager);
        $breeds = [
            "PERRO",
            "GATO",
            "TORTUGA",
            "HÁMSTER"
        ];
        foreach ($breeds as $breed) {
            $this->createCatalog($manager, 'BREED', $breed);
        }
        $manager->flush();
    }

    private function createUsers(ObjectManager $manager)
    {
        $user = $manager->getRepository(User::class)->findOneBy(['email' => self::USER_ADMIN]);
        if (!$user) {
            $user = new User();
            $user->setRoles(['ROLE_ADMIN']);
            $user->setEmail(self::USER_ADMIN);
            $user->setPassword($this->encoder->encodePassword($user, 123456));
            $manager->persist($user);
        }

    }

    private function createCatalog(ObjectManager $manager, $keyName, $keyValue)
    {
        $catalog = $manager->getRepository(Catalog::class)->findOneBy(['keyName' => $keyName, 'keyValue' => $keyValue]);
        if (!$catalog) {
            $catalog = new Catalog();
            $catalog->setKeyName($keyName)
                ->setKeyValue($keyValue);
            $manager->persist($catalog);
        }
    }
}
