<?php

namespace App\Controller;

use App\Entity\Catalog;
use App\Entity\Pets;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="app_main")
     */
    public function index(): Response
    {
        return $this->render('main/index.html.twig', [

        ]);
    }

    /**
     * @Route("/registry/pet", name="app_registry_pet", methods={"GET"})
     */
    public function registryPet(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $breed = $em->getRepository(Catalog::class)->findBy(['keyName' => 'BREED']);
        return $this->render('main/pet-registry.html.twig', [
            'breeds' => $breed
        ]);
    }

    /**
     * @Route("/registry/pet", methods={"POST"})
     */
    public function registryPetSave(Request $request, FileUploader $service): Response
    {
        $em = $this->getDoctrine()->getManager();
        $pet = new Pets();
        $breed = $em->getRepository(Catalog::class)->find($request->get('breed'));
        $file = $request->files->get('image');
        $uploaded = "";
        if ($file) $uploaded = $service->getTargetDirectory() . $service->upload($file);
        $pet->setBirthdate(\DateTime::createFromFormat('Y-m-d',$request->get('birthdate')))
            ->setBreedCatalog($breed)
            ->setDescription($request->get('description'))
            ->setOwnerName($request->get('ownerName'))
            ->setPetName($request->get('namePet'))
            ->setImage($uploaded);
        $em->persist($pet);
        $em->flush();
        $this->addFlash('success', 'Datos Guardados');

        return $this->redirectToRoute('app_main');
    }
}
